# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 10:30:07 2021

@author: yannis
"""


import math
import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt

np.set_printoptions(linewidth=240)

def centrage(X):    
    return(X-np.mean(X,0))

A = np.array ([
    [3, 97.8, 119, 2.6, 7.4, 0.2, 1.8, 2.2, 53, 34, 28.3, 21],
    [0.2, 80.5, 121, 2.1, 5.3, 0.5, 1.4, 1.4, 47.4, 20.5, 27.8, 19.9],
    [0.4, 6.1, 67, 4.2, 9.9, 3, 1.5, 2.5, 10.6, 21, 15.1, 23.1],
    [7.3, 106.4, 129, 1.9, 14.7, 1.1, 2.1, 3.7, 45.9, 12.5, 22.1, 29.9],
    [4.6, 170.6, 79, 1, 26.4, -4.4, 1.4, 11.8, 27.6, 23, 26, 31],
    [6.7, 69.3, 98, 2.4, 26.2, -1.4, 1.4, 4.9, 32.7, 35, 22.7, 27],
    [3.7, 86, 108, 2.2, 10.6, 0.1, 2, 2, 45.4, 34, 30.8, 19.3],
    [2.1, 120.7, 100, 3.3, 11.7, -1, 1.4, 4.6, 35.6, 33, 27.8, 24.5],
    [4.5, 71.1, 94, 3.1, 14.7, -3.5, 1.4, 12, 20.7, 10, 18.4, 23.5],
    [0.9, 18.3, 271, 2.9, 5.3, 0.5, 1.5, 2.3, 50.7, 22, 20.1, 16.8], 
    [2.9, 70.9, 85, 3.2, 7, 1.5, 1.5, 4, 25, 35, 18.9, 21.4],
    [3.6, 65.5, 131, 2.8, 6, -0.6, 1.8, 1.6, 55.7, 34, 28.4, 15.7],
    [2.5, 72.4, 129, 2.6, 4.9, 0.7, 1.4, 1.7, 45.6, 34, 28.2, 16.9],
    [4.9, 108.1, 77, 2.8, 17.6, -1.9, 1.4, 6, 14.8, 25, 24.3, 24.4],
    [5.1, 46.9, 84, 2.8, 10.2, -2, 1.6, 4.9, 18.5, 20, 21.5, 19.3],
    [3.3, 43.3, 73, 3.7, 14.9, 1.1, 1.5, 2.1, 10.1, 19, 16, 20.6],
    [1.5, 49, 114, 3.2, 7.9, 0.3, 1.8, 1.6, 46.8, 26, 26.3, 17.9]],
    dtype=np.float64)
"""
print(A)
A=np.array([[1,21,3],[4,5,12],[11,8,30]])
"""
m, n = A.shape

A_centre=centrage(A)
"""print(A_centre)"""

def reduit(X):
    ecart=np.diag(1/np.std(X,0))
    return(np.dot(X,ecart))

A_reduit=reduit(A_centre)
"""print(A_reduit)"""

def centre_reduit(X):
    return(reduit(centrage(X)))

A_cr=centre_reduit(A)
"""print(A_cr)"""

def covariance(X):
    return(np.cov(X,bias=True))

Cov_A=covariance(A_cr)
"""print(Cov_A)"""
X2=nla.eig(Cov_A)

val_propre = X2[0]
vect_propre = X2[1]


def pos_max_2(X):
    max1=X[0]
    posMax1=0
    
    for i in range(1,len(X)):
        if (max1<X[i]):
            max1=X[i]
            posMax1=i
    if (posMax1!=0) :
        max2=X[0]
        posMax2=0
    else:
        max2=X[1]
        posMax2=1
    for j in range(1,len(X)):
        if (max2<X[j] and j!=posMax1):
            max2=X[j]
            posMax2=j
    return (posMax1,posMax2)

posMax=pos_max_2(np.abs(val_propre))
"""
print(vect_propre[0:vect_propre.shape[0],posMax[0]])
print(vect_propre[0:vect_propre.shape[0],posMax[1]])
        
print(val_propre[1])
print(vect_propre[1])
"""

P=np.transpose(np.array([vect_propre[0:vect_propre.shape[0],posMax[0]],vect_propre[0:vect_propre.shape[0],posMax[1]]]))
print(P)

lignes = ['BE', 'DE', 'EE', 'IE', 'EL', 'ES', 'FR', 'IT', 'CY', 'LU', 'MT', 'NL', 'AT', 'PT', 'SL', 'SK', 'FI']
colonnes = [
'Déficit public / PIB (estimation 2013)', 
'Dette / PIB en %', 
'PIB / habitant', 
'inflation fin 2012', 
'taux de chômage M01 en 2013', 
'taux de croissance du PIB réel (prévision 2013)', 
'taux de fécondité', 
'taux d\'emprunt à 10 ans', 
'coût main d\'œuvre / produits manufacturés', 
'impôt sur les sociétés', 
'prélèvements sociaux', 
'% de pauvreté ou d\'exclusion sociale']

P[0:17,0]=-P[0:17,0]

plt.scatter(P[0:17,0],P[0:17,1])

print("pays | ", colonnes[posMax[0]]," | ",colonnes[posMax[1]])
for i in range(0,len(P)):
    plt.annotate (lignes[i], P[i,:])
    
"""deuxieme partie"""

u, s, vh = nla.svd(A_cr, full_matrices=False)


smat = np.diag(s)
Abis=np.dot(u, np.dot(smat, vh))

posMax2=pos_max_2(np.abs(s))
X2=np.array([u[0:u.shape[0],posMax2[0]],u[0:u.shape[0],posMax2[1]]])
P2=np.transpose(X2)
print(P2)



plt.scatter(P2[0:17,0],P2[0:17,1])

print("pays | ", colonnes[posMax2[0]]," | ",colonnes[posMax2[1]])
for i in range(0,len(P2)):
    plt.annotate (lignes[i], P2[i,:])
    
###### partie 3 #######

# Affichage plus agréable
np.set_printoptions(linewidth=240)

def reflecteur(p, v):
    n = v.shape[0]
    F = np.eye(n) - 2 * np.outer(v,v)
    Q = np.eye(p, dtype=np.float64)
    Q[p-n:p,p-n:p] = F
    return Q

A = np.array ([
    [3, 97.8, 119, 2.6, 7.4, 0.2, 1.8, 2.2, 53, 34, 28.3, 21],
    [0.2, 80.5, 121, 2.1, 5.3, 0.5, 1.4, 1.4, 47.4, 20.5, 27.8, 19.9],
    [0.4, 6.1, 67, 4.2, 9.9, 3, 1.5, 2.5, 10.6, 21, 15.1, 23.1],
    [7.3, 106.4, 129, 1.9, 14.7, 1.1, 2.1, 3.7, 45.9, 12.5, 22.1, 29.9],
    [4.6, 170.6, 79, 1, 26.4, -4.4, 1.4, 11.8, 27.6, 23, 26, 31],
    [6.7, 69.3, 98, 2.4, 26.2, -1.4, 1.4, 4.9, 32.7, 35, 22.7, 27],
    [3.7, 86, 108, 2.2, 10.6, 0.1, 2, 2, 45.4, 34, 30.8, 19.3],
    [2.1, 120.7, 100, 3.3, 11.7, -1, 1.4, 4.6, 35.6, 33, 27.8, 24.5],
    [4.5, 71.1, 94, 3.1, 14.7, -3.5, 1.4, 12, 20.7, 10, 18.4, 23.5],
    [0.9, 18.3, 271, 2.9, 5.3, 0.5, 1.5, 2.3, 50.7, 22, 20.1, 16.8], 
    [2.9, 70.9, 85, 3.2, 7, 1.5, 1.5, 4, 25, 35, 18.9, 21.4],
    [3.6, 65.5, 131, 2.8, 6, -0.6, 1.8, 1.6, 55.7, 34, 28.4, 15.7],
    [2.5, 72.4, 129, 2.6, 4.9, 0.7, 1.4, 1.7, 45.6, 34, 28.2, 16.9],
    [4.9, 108.1, 77, 2.8, 17.6, -1.9, 1.4, 6, 14.8, 25, 24.3, 24.4],
    [5.1, 46.9, 84, 2.8, 10.2, -2, 1.6, 4.9, 18.5, 20, 21.5, 19.3],
    [3.3, 43.3, 73, 3.7, 14.9, 1.1, 1.5, 2.1, 10.1, 19, 16, 20.6],
    [1.5, 49, 114, 3.2, 7.9, 0.3, 1.8, 1.6, 46.8, 26, 26.3, 17.9]],
    dtype=np.float64)

m, n = A.shape

# Méthode moderne

for j in range(n):
    moy = np.mean(A[:,j]) 
    for i in range(m):
        A[i,j] = A[i,j]- moy


for j in range(n):
    s = math.sqrt((1/m)*np.dot(np.transpose(A[:,j]),A[:,j]))
    for i in range(m):
        A[i,j] = A[i,j]/s
        
B = np.copy(A)

x = B[0:m,0]
v = np.copy(x)
v[0] = v[0] + np.sign(v[0]) * nla.norm(x,2)
v = (1/nla.norm(v,2)) * v
VL = [v]
Q = reflecteur(m,v)
B = np.dot(Q,B)

x = B[0,1:n]
v = np.copy(x)
v[0] = v[0] + np.sign(v[0]) * nla.norm(x,2)
v = (1/nla.norm(v,2)) * v
VR = [v]
Q = reflecteur(n,v)
B = np.dot(B,Q)

for i in range(1,n-1):
    x = B[i:m,i]
    v = np.copy(x)
    v[0] = v[0] + np.sign(v[0]) * nla.norm(x,2)
    v = (1/nla.norm(v,2)) * v
    VL.append(v)
    Q = reflecteur(m,v)
    B = np.dot(Q,B)

    x = B[i,i+1:n]
    v = np.copy(x)
    v[0] = v[0] + np.sign(v[0]) * nla.norm(x,2)
    v = (1/nla.norm(v,2)) * v
    VR.append(v)
    Q = reflecteur(n,v)
    B = np.dot(B,Q)
    
x = B[n-1:m,n-1]
v = np.copy(x)
v[0] = v[0] + np.sign(v[0]) * nla.norm(x,2)
v = (1/nla.norm(v,2)) * v
VL.append(v)
Q = reflecteur(m,v)
B = np.dot(Q,B)
        
B = B[0:n,0:n]



H = np.zeros([2*n, 2*n], dtype=np.float64)
H[0:n,n:2*n] = np.transpose(B)
H[n:2*n,0:n] = B

P = np.zeros([2*n,2*n], dtype=np.float64)
for i in range (0,n):
    P[i,2*i] = 1
    P[n+i,2*i+1] = 1
    
T = np.dot(np.transpose(P), np.dot(H,P))

d = np.zeros(2*n, dtype=np.float64)
e = np.array([T[i+1,i] for i in range (0,2*n-1)], dtype=np.float64)
eigvals, eigvecs = nla.eigh_tridiagonal(d,e)
Lambda = eigvals[n:2*n]
Q = eigvecs[:,n:2*n]

Y = np.sqrt(2) * np.dot(P, Q)

newVt = np.transpose(Y[0:n,:])
newU = np.zeros([m,n], dtype=np.float64)
newU[0:n,:] = Y[n:2*n,:]

newSigma = np.array(np.diag(Lambda), dtype=np.float64)

for i in range (n-1, -1, -1):
    Q = reflecteur(m, VL[i])
    newU = np.dot(Q, newU)
   
for i in range(n-2, -1, -1):
    Q = reflecteur(n, VR[i])
    newVt = np.dot(newVt, Q)
    
max1 = -1
max2 = -1
imax1 = -1
imax2 = -1


for i in range (np.size(Lambda)) :
    if (abs(Lambda[i])> max1) :
        max1 = abs(Lambda[i])
        imax1 = i
        
for i in range (np.size(Lambda)) :
    if ((abs(Lambda[i]) > max2) and (abs(Lambda[i]) < max1)):
        max2 = abs(Lambda[i])
        imax2 = i

v1 = newVt[imax1]
v2 = newVt[imax2]

v1 = v1.reshape(-1, 1)
v2 = v2.reshape(-1, 1)

v12 = np.concatenate((v1, v2), axis=1)

P = -1 * np.dot(A, v12)

lignes = ['BE', 'DE', 'EE', 'IE', 'EL', 'ES', 'FR', 'IT', 'CY', 'LU', 'MT', 'NL', 'AT', 'PT', 'SL', 'SK', 'FI']
colonnes = [
'Déficit public / PIB (estimation 2013)', 
'Dette / PIB en %', 
'PIB / habitant', 
'inflation fin 2012', 
'taux de chômage M01 en 2013', 
'taux de croissance du PIB réel (prévision 2013)', 
'taux de fécondité', 
'taux d\'emprunt à 10 ans', 
'coût main d\'œuvre / produits manufacturés', 
'impôt sur les sociétés', 
'prélèvements sociaux', 
'% de pauvreté ou d\'exclusion sociale']

plt.scatter (P[:,0], P[:,1])
for i in range (0,m) :
    plt.annotate (lignes[i], P[i,:])

plt.scatter (0, 0, color='white')

plt.show ()



