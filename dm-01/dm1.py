# -*- coding: utf-8 -*-
"""
Created on Sat Apr 10 13:52:22 2021

@author: yanni

----- Q1 -----

def f1(x):
    return (x*x)-4

def df1(x):
    return 2*x

u1=[3.0]
    
for i in range(6):
    u1.append(u1[i]- (f1(u1[i])/df1(u1[i])))
    
print(u1)
    
on a bien u rempli comme dans l'exemple

u2=[-3.0]
        
for i in range(6):
    u2.append(u2[i]- (f1(u2[i])/df1(u2[i])))
        
print(u2)
    
si u[0]<0 on converge vers -2 au lieu de converger vers 2

def f2(x):
    return pow(x-2,2)

def df2(x):
    return (2*x)-4

u3=[3.0]
        
for i in range(30):
    u3.append(u3[i]- (f2(u3[i])/df2(u3[i])))
        
print(u3)
    
    on converge encore vers 2 mais moins rapidement
   
def f3(x):
    return pow(x,2)-3

def df3(x):
    return (2*x)

u4=[3.0]
        
for i in range(6):
    u4.append(u4[i]- (f3(u4[i])/df3(u4[i])))
        
print(u4)

----- Q2 -----

import numpy as np
import scipy.linalg as nla

def f1(x,y):
    return pow(x,2)+2*x*y-1

def f2(x,y):
    return pow(x,2)*pow(y,2)-y-3

def test1(x,y):
    return np.array([f1(x,y),f2(x,y)])


    print(test1(2,-3/4))
   
    on a bien [0,0]

def jacobien(x,y):
    return np.array([[(2*x)+(2*y),2*x],[2*x*pow(y,2),(2*y*pow(x,2))-1]])

def test2(x,y):
    return np.array([-f1(x,y),-f2(x,y)])

u=np.array([3.0,-1])
print(u)

for i in range(6):
    J=jacobien(u[0], u[1])
    m=test2(u[0], u[1])
    
    Q,R = nla.qr(J)
    x=nla.solve_triangular(R,np.dot(np.transpose(Q),m),lower=False)
    u[0]=u[0]+x[0]
    u[1]=u[1]+x[1]
    print(u)
 """   
    
    
